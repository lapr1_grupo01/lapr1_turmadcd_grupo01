package projetofinallapr1;

import java.util.Formatter;

/**
 * Métodos que não são específicos aos métodos matemáticos
 */
public class Utilitarios {

    private static final int MIN_MATRIZ_AHP = 0;
    private static final int MAX_MATRIZ_AHP = 0;

    /**
     * Escreve a matriz fornecida
     *
     * @param out - Formatter para onde se escreve a matriz
     * @param matriz - matriz que se quer escrever
     */
    public static void escreverMatriz(Formatter out, double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                out.format(matriz[i][j] + " ");
            }
            out.format("%n");
        }
    }

    /**
     * Escreve a matriz fornecida
     *
     * @param out - Formatter para onde se escreve a matriz
     * @param matriz - matriz que se quer escrever
     */
    public static void escreverMatrizSemZeros(Formatter out, double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if(matriz[i][j]!=0){
                out.format(matriz[i][j] + " ");
                }
            }
            out.format("%n");
        }
    }

    /**
     * Escreve a matriz de Strings fornecida
     *
     * @param out - Formatter para onde se escreve a matriz
     * @param matriz - matriz de Strings que se quer escrever
     */
    public static void escreverMatrizStrings(Formatter out, String[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                out.format(matriz[i][j] + " ");
            }
            out.format("%n");
        }
    }

    /**
     * Escreve o vetor fornecido
     *
     * @param out - Formatter para onde se escreve o vetor
     * @param vetor - vetor que se quer escrever
     */
    public static void escreverVetor(Formatter out, double[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            out.format(vetor[i] + " ");
        }
        out.format("%n");
    }

    /**
     * Escreve o vetor fornecido com um tipo de formatação específico
     *
     * @param out - Formatter para onde se escreve o vetor
     * @param vetor - vetor que se quer escrever
     */
    public static void escreverVetorFormat(Formatter out, double[] vetor) {

        String[] formatacao = {"%4s", "%9s", "%12s", "%6s"};
        for (int i = 0; i < vetor.length; i++) {
            out.format(formatacao[i] + " ", vetor[i]);
        }
        out.format("%n");
    }

    /**
     * Escreve o vetor de Strings fornecido
     *
     * @param out - Formatter para onde se escreve o vetor
     * @param vetor - vetor de Strings que se quer escrever
     */
    public static void escreverVetorStrings(Formatter out, String[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            out.format(vetor[i] + " ");
        }
        out.format("%n");
    }

    /**
     * Escreve o vetor de Strings fornecido com um tipo de formatação específico
     *
     * @param out - Formatter para onde se escreve o vetor
     * @param vetor - vetor de Strings que se quer escrever
     */
    public static void escreverVetorStringsFormat(Formatter out, String[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            out.format("%-18s ", vetor[i]);
        }
        out.format("%n");
    }

    /**
     * Escreve o vetor de Chars fornecido
     *
     * @param out - Formatter para onde se escreve o vetor
     * @param vetor - vetor de Chars que se quer escrever
     */
    public static void escreverVetorChars(Formatter out, char[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            out.format(vetor[i] + " ");
        }
        out.format("%n");
    }

    /**
     * Multiplica uma matriz por um vetor
     *
     * @param matriz - matriz a multiplicar
     * @param vetor - vetor a multiplicar
     * @return o vetor resultante da multiplicação
     */
    public static double[] multiplicacaoMatrizPorVetor(double[][] matriz, double[] vetor) {

        double[] vetorResultado = new double[matriz.length];
        for (int i = 0; i < matriz.length; i++) {

            double soma = 0;
            for (int j = 0; j < vetor.length; j++) {
                soma = soma + (vetor[j] * matriz[i][j]);
            }
            vetorResultado[i] = soma;
        }
        return vetorResultado;
    }

    /**
     * Soma os elementos de uma coluna de uma matriz
     *
     * @param matriz - matriz de qual se quer somar os elementos de uma coluna
     * @param coluna - coluna a somar
     * @return a soma
     */
    public static double somaColuna(double[][] matriz, int coluna) {
        double soma = 0;
        for (int i = 0; i < matriz.length; i++) {
            soma += matriz[i][coluna];
        }
        return soma;
    }

    /**
     * Soma os elementos de uma linha de uma matriz
     *
     * @param matriz - matriz de qual se quer somar os elementos de uma linha
     * @param linha - linha a somar
     * @return a soma da linha
     */
    public static double somaLinha(double[][] matriz, int linha) {
        double soma = 0;
        for (int j = 0; j < matriz.length; j++) {
            soma += matriz[linha][j];
        }
        return soma;
    }

    /**
     * Converte uma String para um double, tendo em conta se a String contém
     * barras, vírgulas ou pontos e se é válida
     *
     * @param str String a ser convertida
     * @return double ou informação de String inválida
     */
    public static double converterStringParaDouble(String str) {
        if (!validacaoDigito(str)) {
            int posPonto = -1;
            for (int caracter = 0; caracter < str.length(); caracter++) {
                if (str.charAt(caracter) == '.' || str.charAt(caracter) == ',') {
                    if (str.charAt(caracter) == ',') {
                        str = str.substring(0, caracter) + '.' + str.substring(caracter + 1);
                    }
                    posPonto = caracter;
                    caracter = str.length();
                }
            }
            if (posPonto == 0) {
                return -1;
            }
            if (posPonto != -1) {
                if (validacaoDigito(str.substring(0, posPonto)) && validacaoDigito(str.substring(posPonto + 1, str.length()))) {
                    return (Double.parseDouble(str));
                } else {
                    return -1;
                }
            } else {
                int posBarra = -1;
                for (int caracter = 0; caracter < str.length(); caracter++) {
                    if (str.charAt(caracter) == '/') {
                        posBarra = caracter;
                        caracter = str.length();
                    }
                }
                if (posBarra == 0) {
                    return -1;
                }
                if (posBarra != -1) {
                    if (validacaoDigito(str.substring(0, posBarra)) && validacaoDigito(str.substring(posBarra + 1, str.length()))) {
                        double antesBarra = Double.parseDouble(str.substring(0, posBarra));
                        double depoisBarra = Double.parseDouble(str.substring(posBarra + 1, str.length()));
                        if (antesBarra / depoisBarra < 10) {
                            return (antesBarra / depoisBarra);
                        } else {
                            return -1;
                        }
                    } else {
                        return -1;
                    }
                }
                return -1;
            }
        } else {
            return Double.parseDouble(str);
        }
    }

    /**
     * Verifica se a string em questao e apenas constituida por digitos
     *
     * @param str - string a ser estudada
     * @return true se for apenas constituida por digitos e false se nao for
     */
    public static boolean validacaoDigito(String str) {
        for (int character = 0; character < str.length(); character++) {
            if (!Character.isDigit(str.charAt(character))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Ordena um vetor de Strings de forma a que todas as Strings vazias fiquem
     * no fim do vetor
     *
     * @param vec - vetor do qual se quer remover Strings vazias
     * @return o número de Strings não vazios que o vetor contém
     */
    public static int removerStringNulaDeVetor(String vec[]) {
        int tamanhoVec = vec.length - 1;
        for (int i = 0; i < tamanhoVec; i++) {
            if (vec[i].equals("")) {
                for (int j = i; j < tamanhoVec; j++) {
                    vec[j] = vec[j + 1];
                }
                i--;
                tamanhoVec--;
            }
        }
        return tamanhoVec + 1;
    }

    /**
     * Guarda os dados de um ficheiro numa matriz
     *
     * @param logErros - Formatter que regista os erros num ficheiro à parte
     * @param linha - linha do ficheiro a analisar
     * @param ordem - dimensão da matriz
     * @param indiceLinha - linha da matriz a preencher
     * @param mat - matriz onde se guardam os dados
     * @param tipoFicheiro - tipo de ficheiro, AHP ou TOPSIS
     * @return 0 se os dados tiverem sido guardados; -1 se os dados forem
     * inválidos
     */
    public static int guardarEmMemoria(Formatter logErros, String linha, int ordem, int indiceLinha, double[][] mat, String tipoFicheiro) {
        String[] stringElementoMatriz = linha.trim().split(" +");

        double[] doubleElementoMatriz = new double[ordem];
        for (int i = 0; i < ordem; i++) {
            doubleElementoMatriz[i] = converterStringParaDouble(stringElementoMatriz[i]);
            if (tipoFicheiro.equals("AHP")) {
                if (doubleElementoMatriz[i] > MAX_MATRIZ_AHP && doubleElementoMatriz[i] <= MIN_MATRIZ_AHP) {
                    return -1;
                }
            }
            if (doubleElementoMatriz[i] == -1) {
                LogErros.registarErro(logErros, "Um dos elementos de uma das matrizes é inválido.");
                return -1;
            }
        }

        for (int i = 0; i < ordem; i++) {
            mat[indiceLinha][i] = doubleElementoMatriz[i];
        }
        return 0;
    }

    /**
     * Converte um vetor de Strings para Doubles
     *
     * @param vetorStrings - vetor de Strings cujos elementos se quer converter
     * em double
     * @return vetor com elementos em double
     */
    public static double[] converterArrayStringParaDouble(String[] vetorStrings) {
        double[] arrayDouble = new double[vetorStrings.length];
        for (int i = 0; i < vetorStrings.length; i++) {
            arrayDouble[i] = converterStringParaDouble(vetorStrings[i]);
        }
        return arrayDouble;
    }

    /**
     * Soma todos os elementos de um vetor
     *
     * @param vetor - vetor do qual se pretende obter a soma
     * @return soma dos elementos do vetor
     */
    public static double somaVetor(double[] vetor) {
        double soma = 0;
        for (int i = 0; i < vetor.length; i++) {
            soma += vetor[i];
        }
        return soma;
    }

    /**
     * Recebe um vetor e normaliza-o
     *
     * @param vetor - o vetor que se quer normalizar
     * @return a versão normalizada do vetor recebido
     */
    public static double[] normalizarVetor(double[] vetor) {

        double[] vProprioNormalizado = new double[vetor.length];
        double somaVetor = Utilitarios.somaVetor(vetor);
        for (int i = 0; i < vetor.length; i++) {
            vProprioNormalizado[i] = vetor[i] / somaVetor;
        }
        return vProprioNormalizado;

    }

    /**
     * Verifica se um dado valor se encontra num vetor
     *
     * @param val - valor estudado
     * @param vec - vetor estudado
     * @return true caso o valor exista no vetor ou false caso não exista
     */
    public static boolean valorPresenteEmVetor(int val, int[] vec) {
        boolean flag = false;
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] == val) {
                flag = true;
                i = vec.length;
            }

        }
        return flag;
    }

}
