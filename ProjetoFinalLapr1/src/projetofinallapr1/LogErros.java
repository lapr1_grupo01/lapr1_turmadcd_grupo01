package projetofinallapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Métodos utilitários associados à criação e escrita de informação num ficheiro
 * de registo de erros
 */
public class LogErros {
    
    public static final String FILE_LOG_ERROS = "Erros.txt";

    public static DateFormat sdf =new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss") ;
    
    /**
     * Cria e abre um ficheiro de texto para escrita
     * @param nomeFile - o nome do ficheiro a criar
     * @return uma objeto de Formatter que referencia o ficheiro criadao
     * @throws FileNotFoundException
     */
    public static Formatter criarEAbrirFileLogErros(String nomeFile) throws FileNotFoundException {
        Formatter outputLogErros = new Formatter(new File(nomeFile));
        return outputLogErros;
    }

    /**
     * Registar um erro numa linha do ficheiro
     * @param outputLogErros - referência para o ficheiro de erros
     * @param tipoErro - tipo de erro identificado nessa linha
     */
    public static void registarErro(Formatter outputLogErros, String tipoErro) {
        Date data = new Date();
        String dataFormat = sdf.format(data);
        System.out.println("Ocorreu um erro, consulte o ficheiro " + FILE_LOG_ERROS);
        outputLogErros.format("[%s] Erro: %s%n", dataFormat, tipoErro);
    }

    /**
     * Fechar o ficheiro de registo de erros
     * @param outputLogErros - referência para o ficheiro de erros
     */
    public static void fecharFileLogErros(Formatter outputLogErros) {
        outputLogErros.close();
    }

    /**
     * Listar para o ecrã um ficheiro de texto
     * @param nomeFile - nome do ficheiro de texto
     * @throws FileNotFoundException
     */
    public static void listarFileLogErros(String nomeFile) throws FileNotFoundException {
        Scanner outputLogErros = new Scanner(new File(nomeFile));
        while (outputLogErros.hasNextLine()) {
            System.out.println(outputLogErros.nextLine());
        }
        outputLogErros.close();
    }

}
