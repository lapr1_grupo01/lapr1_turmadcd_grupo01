package projetofinallapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import org.la4j.*;
import org.la4j.decomposition.EigenDecompositor;

/**
 * Leitura de dados e processamento relativo ao método
 * matemático AHP
 */
public class AHP {

    public static final double[] TABELA_IR = {0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};
    private final static int MIN_CRITERIOS = 2;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //TESTES DA LEITURA DO FICHEIRO
        //        for (int i = 0; i < numCriterios; i++) {
        //            for (int j = 0; j < numCriterios; j++) {
        //                System.out.print(vetorMatrizesComparacao[0][i][j]);
        //            }
        //            System.out.print("\n");
        //        }
        //        System.out.println("\n");
        //        for (int i = 1; i < numCriterios+1; i++) {
        //            
        //            for (int j = 0; j < 4; j++) {
        //                for (int k = 0; k < 4; k++) {
        //                    System.out.print(vetorMatrizesComparacao[i][j][k]);
        //                }
        //                System.out.print("\n");
        //            }
        //            System.out.print("\n");
        //            System.out.print("\n");
        //        }
        //        for (int i = 0; i < numAlternativas; i++) {   
        //            System.out.println(nomesAlternativas[i]);
        //        }
        //        System.out.println("\n");
        //        for (int i = 0; i < numCriterios; i++) {
        //            System.out.println(nomesCriterios[i]);
        //        }
    }

    /**
     * Cria a matriz dos criterios
     *
     * @param nomeFicheiro - nome do ficheiro a ser lido
     * @param logErros - Formatter que regista os erros num ficheiro à parte
     * @param tipoFicheiro - String que guarda o método a usar (AHP ou TOPSIS)
     * @return a matriz dos critérios em double
     * @throws FileNotFoundException
     */
    public static double[][] criarMatrizCriterios(String nomeFicheiro, Formatter logErros, String tipoFicheiro) throws FileNotFoundException {
        Scanner inputF = new Scanner(new File(nomeFicheiro));
        double[][] matrizCriterios = null;
        int var = 0, linhaMatriz = 0;
        while (inputF.hasNext()) {
            String linha = inputF.nextLine();
            if (linha.trim().split("  ")[0].equals("mc_criterios")) {

                int numCriterios = linha.trim().split("  ").length - 1;
                matrizCriterios = new double[numCriterios][numCriterios];
                for (int i = 0; i < numCriterios; i++) {
                    linha = inputF.nextLine();
                    Utilitarios.guardarEmMemoria(logErros, linha, numCriterios, i, matrizCriterios, tipoFicheiro);
                }

            }
        }
        return matrizCriterios;

    }

    /**
     * Analisa o ficheiro de input para descobrir o número de alternativas
     *
     * @param nomeFicheiro - nome do ficheiro a ser lido
     * @return o número de alternativas
     * @throws FileNotFoundException
     */
    public static int numAlternativas(String nomeFicheiro) throws FileNotFoundException {
        Scanner inputF = new Scanner(new File(nomeFicheiro));
        int varAlt = 0, numAlt = 0;
        while (inputF.hasNext()) {
            String linha = inputF.nextLine();
            if (linha.length() > 0) {
                if (linha.trim().charAt(0) == 'm') {
                    if (linha.trim().charAt(2) == 'p' && varAlt == 0) {
                        numAlt = linha.trim().split("  ").length - 1;
                        varAlt = 1;
                    }
                }
            }
        }
        return numAlt;
    }

    /**
     * Lê o ficheiro dado, preenche as matrizes com a informação do ficheiro,
     * ignorando as linhas vazias
     *
     * @param logErros - Formatter que regista os erros num ficheiro à parte
     * @param vetorMatrizesComparacao - vetor que contém as matrizes de
     * comparação
     * @param nomesCriterios - vetor vazio onde vao ser colocados os nomes do
     * critérios
     * @param nomesAlternativas - vetor vazio onde vao ser colocados os nomes
     * das alternativas passado
     * @param nomeFicheiro - nome do ficheiro a ser lido
     * @param tipoFicheiro - String que guarda o método a usar (AHP ou TOPSIS)
     * no ficheiro a ler lido (usado para verificar se os números no AHP estão
     * entre 0 e 9)
     * @return a validade do ficheiro (se contém todos os dados necessários ou
     * não e se algum desses dados é inválido)
     * @throws FileNotFoundException
     */
    public static boolean leituraDados(Formatter logErros, double[][][] vetorMatrizesComparacao, String[] nomesCriterios, String[] nomesAlternativas, String nomeFicheiro, String tipoFicheiro) throws FileNotFoundException {

        boolean alternativaJaLida = false;

        Scanner inputF = new Scanner(new File(nomeFicheiro));

        while (inputF.hasNext()) {
            String linha = inputF.nextLine();

            if (linha.length() > 0) {

                if (linha.trim().substring(0, 3).equals("mc_")) {

                    for (int i = 0; i < nomesCriterios.length; i++) {
                        String[] elementosLinha = linha.trim().split("  ");
                        nomesCriterios[i] = elementosLinha[i + 1];
                    }
                } else if (linha.trim().substring(0, 3).equals("mcp")) {

                    if (!alternativaJaLida) {
                        for (int i = 0; i < nomesAlternativas.length; i++) {
                            String[] elementosLinha = linha.trim().split("  ");
                            nomesAlternativas[i] = elementosLinha[i + 1];
                            alternativaJaLida = true;
                        }
                    }

                    String primeiroElementoLinha = linha.split("  ")[0];
                    String nomeCriterio = primeiroElementoLinha.substring(4);
                    int indiceVetorMatrizesComparacao = indiceMatrizDoCriterio(nomesCriterios, nomeCriterio, logErros);
//                    System.out.println(nomesAlternativas.length);
//                    System.out.println(indiceVetorMatrizesComparacao+"--");
                    if (indiceVetorMatrizesComparacao == -1) {
                        return false;
                    }
                    vetorMatrizesComparacao[indiceVetorMatrizesComparacao] = new double[nomesAlternativas.length][nomesAlternativas.length];
                    for (int i = 0; i < nomesAlternativas.length; i++) {
                        linha = inputF.nextLine();
                        if (indiceVetorMatrizesComparacao != -1) {

                            if (Utilitarios.guardarEmMemoria(logErros, linha, nomesAlternativas.length, i, vetorMatrizesComparacao[indiceVetorMatrizesComparacao], tipoFicheiro) == -1) {
                                return false;
                            }
                        } else {
                            return false;
                        }

                    }
                }
            }
        }
        return true;
    }

    public static int indiceMatrizDoCriterio(String[] nomesCriterios, String nomeCriterio, Formatter logErros) {

        for (int i = 0; i < nomesCriterios.length; i++) {
            if (nomeCriterio.equalsIgnoreCase(nomesCriterios[i])) {
                return i + 1;
            }
        }
        LogErros.registarErro(logErros, "Os critérios listados no cabeçalho da matriz de critérios não correspondem aos nomes dos critérios das matrizes seguintes");
        return -1;
    }

    /**
     * Indica a coluna do maior valor próprio na dos valores proprios criada
     * pelo EigenDecompositor
     *
     * @param vd - matrizes dos valores próprios e dos vetores próprios
     * @return a coluna do maior valor próprio
     */
    public static int colunaMaiorValorProprio(Matrix[] vd) {

        int coluna = 0;
        for (int i = 1; i < vd[1].rows(); i++) {

            if (vd[1].get(i, i) > vd[1].get(coluna, coluna)) {

                coluna = i;

            }

        }
        return coluna;
    }

    /**
     * Analisa a matriz dos vetores próprios e atribui a coluna do maior valor
     * próprio a um novo vetor
     *
     * @param vd - matrizes dos valores próprios e dos vetores próprios
     * @param coluna - coluna do maior valor próprio
     * @return - vetor próprio da matriz de comparação
     */
    public static double[] vetorProprio(Matrix[] vd, int coluna) {

        double[] vProprio = new double[vd[1].rows()];

        for (int i = 0; i < vd[0].rows(); i++) {
            vProprio[i] = vd[0].get(i, coluna);
        }

        return vProprio;
    }

    /**
     * Junta os métodos anteriores, criando um método que obtém um vetor próprio
     * normalizado a partir das matrizes e criadas pelo EigenDecompositor
     *
     * @param vd - matrizes dos valores próprios e dos vetores próprios
     * @return o vetor normalizado
     */
    public static double[] obterVetorProprioNormalizado(Matrix[] vd) {

        int coluna = colunaMaiorValorProprio(vd);
        double[] vProprio = vetorProprio(vd, coluna);
        double[] vProprioNormalizado = Utilitarios.normalizarVetor(vProprio);

        return vProprioNormalizado;
    }

    /**
     * Adiciona um vetor próprio à matriz prioridades
     *
     * @param numAlternativas - número de alternativas a comparar
     * @param i - coluna da matriz prioridade a preencher
     * @param mPrioridade - matriz das prioridades relativas
     * @param vProprioNormalizado - vetor próprio a adicionar à matriz
     * @return a matriz atualizada
     */
    public static double[][] criarEPreencherMatrizPrioridade(int numAlternativas, int i, double[][] mPrioridade, double[] vProprioNormalizado) {

        for (int j = 0; j < numAlternativas; j++) {
            mPrioridade[j][i - 1] = vProprioNormalizado[j];
        }

        return mPrioridade;
    }

    /**
     * Cria a matriz Prioridades e o vetor dos pesos dos critérios, tendo em
     * conta se são cálculos exatos e aproximados
     *
     * @param matrizes - vetor que tem todas as matrizes de comparação
     * @param numCriterios - número de critérios dos quais a as alternativas
     * dependem
     * @param numAlternativas - número de alternativas a comparar
     * @param vPesoCriterios - vetor próprio da matriz de comparação de
     * critérios
     * @param mPrioridade - matriz das prioridades relativas
     * @param opcao - escolha do utilizador de usar cálculo exato ou aproximado
     * @param limiar - limiar dos critérios
     * @param vetorIndicesCriteriosCortados vetor com os indices dos criterios
     * cortados por estarem abaixo do limiar
     * @param nomesCriterios vetor com os nomes dos critérios
     * @return a matriz prioridades atualizada
     */
    public static double[][] vetorPesoCriteriosEMatrizPrioridade(double[][][] matrizes, int numCriterios, int numAlternativas, double[] vPesoCriterios, double[][] mPrioridade, String opcao, double limiar, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios) {
        int var = 0, i;
        for (i = 0; i < numCriterios + 1; i++) {
            double[][] normalizada = normalizada(matrizes[i]);
            if (opcao.equalsIgnoreCase("e")) {
                Matrix matrizComparacao = Matrix.from2DArray(matrizes[i]);
                EigenDecompositor decompositor = new EigenDecompositor(matrizComparacao);
                Matrix vd[] = decompositor.decompose();

                if (i == 0) {

                    for (int j = 0; j < numCriterios; j++) {
                        vPesoCriterios[j] = obterVetorProprioNormalizado(vd)[j];
                    }
                    vPesoCriterios = aplicaLimiarCriterios(normalizada, limiar, numCriterios, vPesoCriterios, vetorIndicesCriteriosCortados, nomesCriterios);

                } else {
                    double[] vProprioNormalizado = obterVetorProprioNormalizado(vd);
                    mPrioridade = criarEPreencherMatrizPrioridade(numAlternativas, i, mPrioridade, vProprioNormalizado);
                }
            } else {
                if (i == 0) {
                    for (int j = 0; j < numCriterios; j++) {
                        vPesoCriterios[j] = vetorPrioridades(normalizada, i, limiar, numCriterios, vetorIndicesCriteriosCortados, nomesCriterios)[j];
                        if (vPesoCriterios[0] == -1) {
                            j = numCriterios;
                            mPrioridade[0][0] = -1;
                            var = 1;
                        }
                    }
                    if (var == 1) {
                        i = numCriterios;
                    }
                } else {
                    double[] vPrioridades = vetorPrioridades(normalizada, i, limiar, numCriterios, vetorIndicesCriteriosCortados, nomesCriterios);
                    mPrioridade = criarEPreencherMatrizPrioridade(numAlternativas, i, mPrioridade, vPrioridades);
                }
            }
        }
        return mPrioridade;
    }

    /**
     * Compara os elementos do vetor de prioridades compostas e retorna os
     * índices do maior
     *
     * @param vPrioridadeComposta - vetor que contém a importância de cada
     * alternativa
     * @return o índice da alternativa escolhida
     */
    public static int indiceAlternativaEscolhida(double[] vPrioridadeComposta) {

        int indice = 0, numeroAlternativasMaximas = 0;
        double maximo = vPrioridadeComposta[0];
        for (int i = 1; i < vPrioridadeComposta.length; i++) {
            if (vPrioridadeComposta[i] > maximo) {
                maximo = vPrioridadeComposta[i];
                indice = i;
            }

        }
        for (int i = 0; i < vPrioridadeComposta.length; i++) {
            if (vPrioridadeComposta[i] == maximo) {
                numeroAlternativasMaximas++;
            }
        }
        if (numeroAlternativasMaximas > 1) {
            indice = -1;
        }
        return indice;
    }

    /**
     * Normaliza todos os elementos de uma matriz
     *
     * @param mComparacao - matriz a normalizar
     * @return a matriz normalizada
     */
    public static double[][] normalizada(double[][] mComparacao) {

        double[][] normalizada = new double[mComparacao.length][mComparacao[0].length];
        for (int i = 0; i < mComparacao.length; i++) {
            for (int j = 0; j < mComparacao[0].length; j++) {
                if(Utilitarios.somaColuna(mComparacao, j)==0){
                    normalizada[i][j]=0;
                }
                else{
                    normalizada[i][j] = mComparacao[i][j] / (Utilitarios.somaColuna(mComparacao, j));
                }
                
            }
        }
        return normalizada;
    }

    /**
     * Obtém o vetor próprio de uma matriz através da sua normalizada e chama o
     * método aplicaLimiarCriterios no caso de ser a primeira matriz
     *
     * @param normalizada - matriz normalizada da matriz de comparaçãoo da qual
     * se quer saber o valor próprio
     * @param indice - indice da matriz recebida (relativo ao vetor que contém
     * as matrizes de comparação)
     * @param limiar - limiar dos critérios
     * @param numCriterios - número de critérios
     * @param vetorIndicesCriteriosCortados vetor com os indices dos criterios
     * cortados por estarem abaixo do limiar
     * @param nomesCriterios - vetor com o nome dos critérios
     * @return o vetor próprio ou -1 caso o número de critérios seja menor que 3
     */
    public static double[] vetorPrioridades(double[][] normalizada, int indice, double limiar, int numCriterios, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios) {

        double[] vPrioridades = new double[normalizada.length];
        int i;
        for (i = 0; i < normalizada.length; i++) {
            vPrioridades[i] = Utilitarios.somaLinha(normalizada, i) / normalizada.length;
        }

        if (indice == 0) {
            if (limiar > 0) {
                vPrioridades = aplicaLimiarCriterios(normalizada, limiar, numCriterios, vPrioridades, vetorIndicesCriteriosCortados, nomesCriterios);
            }
        }

        return vPrioridades;
    }

    /**
     * Percorre o vetor proprio recebido, verificando se os seus valores são
     * inferiores ao limiar, e se tal acontecer transforma-os em 0, procedendo
     * de seguida à normalização deste vetorProprio. Escreve os atributos
     * cortados
     *
     * @param normalizada - matriz normalizada da matriz de comparaçãoo da qual
     * se quer saber o valor próprio
     * @param limiar - limiar dos critérios
     * @param numCriterios - número de critérios
     * @param vetorProprio - vetor proprio ao qual vai ser aplicado o limiar dos
     * Criterios
     * @param vetorIndicesCriteriosCortados vetor com os indices dos criterios
     * cortados por estarem abaixo do limiar
     * @param nomesCriterios - vetor com o nome dos critérios
     * @return vetorProprio normalizado e de acordo com o limiar de critérios
     */
    public static double[] aplicaLimiarCriterios(double[][] normalizada, double limiar, int numCriterios, double[] vetorProprio, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios) {
        int i;
        int cont = 0;
        double soma = 0;
        for (i = 0; i < normalizada.length; i++) {
            if (vetorProprio[i] < limiar) {

                vetorProprio[i] = 0;
                vetorIndicesCriteriosCortados[cont] = i;
                cont++;
            }
        }
        if (numCriterios - cont < MIN_CRITERIOS) {
            vetorProprio[0] = -1;
            return vetorProprio;
        }

        for (i = 0; i < normalizada.length; i++) {
            soma += vetorProprio[i];
        }
        for (i = 0; i < normalizada.length; i++) {
            vetorProprio[i] = vetorProprio[i] / soma;
        }

        return vetorProprio;
    }

    /**
     * Obtém o valor próprio de uma matriz
     *
     * @param numAlternativas - número de alternativas a comparar
     * @param numCriterios - número de critérios dos quais a as alternativas
     * dependem
     * @param matriz - matriz da qual se quer obter o valor próprio
     * @param vetorProprio - vetor próprio da matriz
     * @return o valor próprio
     */
    public static double valorProprio(int numAlternativas, int numCriterios, double[][] matriz, double[] vetorProprio) {
        double soma = 0, cont = 0;
        for (int i = 0; i < matriz.length; i++) {
            if (vetorProprio[i] != 0) {
                soma += Utilitarios.multiplicacaoMatrizPorVetor(matriz, vetorProprio)[i] / vetorProprio[i];
                cont++;

            }
        }

        double media = soma / cont;

        return media;
    }

    /**
     * Calcula do índice de consistência de uma matriz
     *
     * @param valorProprio - valor próprio dessa matriz
     * @param dimensaoMatriz - dimensão dessa matriz
     * @return o IC
     */
    public static double calculoIC(double valorProprio, int dimensaoMatriz) {
        double ic = (valorProprio - dimensaoMatriz) / (dimensaoMatriz - 1);
        return ic;
    }

    /**
     * Calcula a razão de consistência de uma matriz
     *
     * @param ic - índice de consistência da matriz
     * @param dimensaoMatriz - dimensão dessa matriz
     * @param limiarConsistencia - limiar que determina a consistência de uma
     * matriz
     * @return o RC
     */
    public static double calculoRC(double ic, int dimensaoMatriz, double limiarConsistencia) {

        double rc = ic / TABELA_IR[dimensaoMatriz - 1];

        if (limiarConsistencia > 0) {
            if (rc > limiarConsistencia) {
                return -1;
            }
        }
        return rc;
    }

    /**
     * Realiza todos os cálculos necessário à utilização do método AHP
     *
     * @param limiarConsistencia - limiar que determina a consistência de uma
     * matriz
     * @param logErros - Formatter que regista os erros num ficheiro à parte
     * @param vetorMatrizesNormalizadas - array tridimensional que contém todas
     * as matrizes de comparação normalizadas
     * @param vetorVetoresPrioridades - array bidimensional que contém o vetor
     * prioridades de cada matriz
     * @param vetorValoresProprios - array que contém o valor próprio de cada
     * matriz
     * @param vetorIC - array que contém o índice de consistência de cada matriz
     * @param vetorRC - array que contém a razão de consistência de cada matriz
     * @param vetorMatrizesComparacao - array tridimensional que contém todas as
     * matrizes de comparação
     * @param numCriterios - número de critérios dos quais as alternativas
     * dependem
     * @param numAlternativas - número de alternativas a comparar
     * @param vPesoCriterios - vetor próprio da matriz de comparação de
     * critérios
     * @param mPrioridade - matriz das prioridades relativas
     * @param opcao - escolha do utilizador de usar cálculo exato ou aproximado
     * @param nomesAlternativas - vetor que contém o nome de cada alternativa
     * @param nomesCriterios - vetor que contém o nome de cada critério
     * @param limiar - limiar dos critérios
     * @param vPrioridadeComposta - vetor que contém a importância de cada
     * alternativa
     * @param vetorIndicesCriteriosCortados vetor com os indices dos criterios
     * cortados por estarem abaixo do limiar
     * @return o índice da alternativa escolhida
     */
    public static int metodoAHP(double limiarConsistencia, Formatter logErros, double[][][] vetorMatrizesNormalizadas, double[][] vetorVetoresPrioridades, double[] vetorValoresProprios, double[] vetorIC, double[] vetorRC, double[][][] vetorMatrizesComparacao, int numCriterios, int numAlternativas, double[] vPesoCriterios, double[][] mPrioridade, String opcao, String[] nomesAlternativas, String[] nomesCriterios, double limiar, double[] vPrioridadeComposta, int[] vetorIndicesCriteriosCortados) {

          vetorMatrizesNormalizadas[0] = normalizada(vetorMatrizesComparacao[0]);

          vetorVetoresPrioridades[0] = vetorPrioridades(vetorMatrizesNormalizadas[0], 0, limiar, numCriterios, vetorIndicesCriteriosCortados, nomesCriterios);

        for (int i = 0; i < numCriterios; i++) {
            for (int j = 0; j < numCriterios; j++) {
                if (Utilitarios.valorPresenteEmVetor(j,vetorIndicesCriteriosCortados) || Utilitarios.valorPresenteEmVetor(i,vetorIndicesCriteriosCortados)) {
                    vetorMatrizesComparacao[0][i][j]=0;
                }
                
            }
            
        }
        
        
        
        
        
        for (int i = 0; i < numCriterios + 1; i++) {

            vetorMatrizesNormalizadas[i] = normalizada(vetorMatrizesComparacao[i]);

            vetorVetoresPrioridades[i] = vetorPrioridades(vetorMatrizesNormalizadas[i], i, limiar, numCriterios, vetorIndicesCriteriosCortados, nomesCriterios);

            vetorValoresProprios[i] = valorProprio(numAlternativas, numCriterios, vetorMatrizesComparacao[i], vetorVetoresPrioridades[i]);

        }

//        if (opcao.equalsIgnoreCase("e") || opcao.equalsIgnoreCase("a")) {
        mPrioridade = vetorPesoCriteriosEMatrizPrioridade(vetorMatrizesComparacao, numCriterios, numAlternativas, vPesoCriterios, mPrioridade, opcao, limiar, vetorIndicesCriteriosCortados, nomesCriterios);
        for (int i = 0; i < numCriterios; i++) {
            if (Utilitarios.valorPresenteEmVetor(i, vetorIndicesCriteriosCortados)) {
                for (int j = 0; j < numAlternativas; j++) {
                    mPrioridade[j][i] = 0;
                }
            }

        }
        int cont=0;
        for (int i = 0; i < vetorIndicesCriteriosCortados.length; i++) {
            if (vetorIndicesCriteriosCortados[i]!=-1) {
                cont++;
            }
            
        }
        if (mPrioridade[0][0] != -1) {
            for (int i = 0; i < numCriterios + 1; i++) {
                vetorIC[i] = calculoIC(vetorValoresProprios[i], numCriterios-cont);
                vetorRC[i] = calculoRC(vetorIC[i],  numCriterios-cont, limiarConsistencia);
                if (vetorRC[i] == -1) {
                    LogErros.registarErro(logErros, "Uma das razões de consistência é inferior ao limiar de consistência");
                    return -1;
                }
            }
        } else {
            LogErros.registarErro(logErros, "O número de critérios que respeitam o limiar é menor que " + MIN_CRITERIOS + ".");
            return -1;
        }
//        } else {
//            LogErros.registarErro(logErros, "O quarto argumento deve ser \"a\"(aproximado) ou \"e\"(exato)!");
//        }   

        for (int i = 0; i < numAlternativas; i++) {
            vPrioridadeComposta[i] = Utilitarios.multiplicacaoMatrizPorVetor(mPrioridade, vPesoCriterios)[i];

        }

//         System.out.println("vetor depois");
//       Utilitarios.escreverVetor(out, vPrioridadeComposta);
        int indice = indiceAlternativaEscolhida(vPrioridadeComposta);
        if (indice == -1) {
            LogErros.registarErro(logErros, "O método não é adequado pois existe mais do que uma alternativa ideal");
        }

        return indice;
    }

}
