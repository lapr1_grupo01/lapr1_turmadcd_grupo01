package projetofinallapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import static projetofinallapr1.Utilitarios.converterStringParaDouble;
import static projetofinallapr1.Utilitarios.escreverMatriz;
import static projetofinallapr1.Utilitarios.escreverVetor;
import static projetofinallapr1.Utilitarios.escreverVetorStrings;
import static projetofinallapr1.Utilitarios.escreverVetorStringsFormat;

/**
 * Recebe da linha de comandos o método a usar (AHP ou TOPSIS),
 * o ficheiro de input, o ficheiro de output e no caso do AHP também o limiar de consistência,
 * o limiar do peso dos critérios e a escolha entre os cálculos exatos ou aproximados
 */
public class ProjetoFinalLapr1 {

    private final static int MIN_CRITERIOS = 3;
    private final static String FILE_LOG_ERROS = "Erros.txt";
    public static final double[] TABELA_IR = {0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};
    static Formatter out = new Formatter(System.out);

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        //String[] argumentos = {"M1", "L0.12", "S5", "a", "input.txt", "output.txt"};
        //String[] argumentos = {"M2", "inputTOPSIS.txt", "output.txt"};
        LogErros.criarEAbrirFileLogErros(FILE_LOG_ERROS);
        Formatter logErros = LogErros.criarEAbrirFileLogErros(FILE_LOG_ERROS);
        String metodo = args[0];

        if (args.length < 3) {
            LogErros.registarErro(logErros, "Não existem argumentos suficientes.");
        } else {
            if (metodo.equalsIgnoreCase("m1")) {
                if (args.length > 6) {

                    LogErros.registarErro(logErros, "Existem argumentos em excesso.");
                } else {
                    String tipoFicheiro = tipoFicheiro(args[args.length - 2]);
                    if (tipoFicheiro.equals("AHP")) {
                        double limiar = -2;
                        double limiarConsistencia = -2;
                        for (int i = 1; i < args.length - 3; i++) {
                            if (args[i].charAt(0) == 'l' || args[i].charAt(0) == 'L') {
                                limiar = converterStringParaDouble(args[i].substring(1));
                            }
                            if (args[i].charAt(0) == 's' || args[i].charAt(0) == 'S') {
                                limiarConsistencia = converterStringParaDouble(args[i].substring(1));
                            }
                        }
                        String exatoOuAproximado = args[args.length - 3];
                        if (!(exatoOuAproximado.equalsIgnoreCase("a") || exatoOuAproximado.equalsIgnoreCase("e"))) {
                            LogErros.registarErro(logErros, "Argumento do valor aproximado ou exato incorreto.");
                        } else {
                            String nomeFicheiro = args[args.length - 2];  //"input.txt"
                            String nomeFicheiroOutput = args[args.length - 1];

                            int numAlternativas = AHP.numAlternativas(nomeFicheiro);

                            if (limiar == -1) {
                                LogErros.registarErro(logErros, "Limiar de critérios inserido inválido.");
                            } else {
                                if (limiarConsistencia == -1) {
                                    LogErros.registarErro(logErros, "Limiar de consistência inserido inválido.");
                                } else {
                                    double[][] matrizCriterios = AHP.criarMatrizCriterios(nomeFicheiro, logErros, tipoFicheiro);
                                    int numCriterios = matrizCriterios.length;

                                    if (numCriterios < MIN_CRITERIOS) {
                                        LogErros.registarErro(logErros, "Todos os critérios foram excluídos com o limiar escolhido.");

                                    } else {

                                        String[] nomesCriterios = new String[numCriterios];
                                        String[] nomesAlternativas = new String[numAlternativas];
                                        double[][][] vetorMatrizesComparacao = new double[numCriterios + 1][][];
                                        vetorMatrizesComparacao[0] = matrizCriterios;

                                        if (AHP.leituraDados(logErros, vetorMatrizesComparacao, nomesCriterios, nomesAlternativas, nomeFicheiro, tipoFicheiro)) {

                                            double[] vPesoCriterios = new double[numCriterios];
                                            double[][] mPrioridade = new double[numAlternativas][numCriterios];
                                            double[] vPrioridadeComposta = new double[numAlternativas];

                                            //double limiarConsistencia = converterStringParaDouble(args[2].substring(1));
                                            double[][][] vetorMatrizesNormalizadas = new double[numCriterios + 1][numAlternativas][numCriterios];
                                            double[][] vetorVetoresPrioridades = new double[numCriterios + 1][numCriterios];
                                            double[] vetorValoresProprios = new double[numCriterios + 1];
                                            double[] vetorIC = new double[numCriterios + 1];
                                            double[] vetorRC = new double[numCriterios + 1];

                                            int[] vetorIndicesCriteriosCortados = new int[numCriterios];
                                            for (int i = 0; i < vetorIndicesCriteriosCortados.length; i++) {
                                                vetorIndicesCriteriosCortados[i] = -1;

                                            }

                                            int indice = AHP.metodoAHP(limiarConsistencia, logErros, vetorMatrizesNormalizadas, vetorVetoresPrioridades, vetorValoresProprios, vetorIC, vetorRC, vetorMatrizesComparacao, numCriterios, numAlternativas, vPesoCriterios, mPrioridade, exatoOuAproximado, nomesAlternativas, nomesCriterios, limiar, vPrioridadeComposta, vetorIndicesCriteriosCortados);
                                            if (indice != -1) {
                                                escreverDadosAHP(indice, nomeFicheiroOutput, numCriterios, nomesCriterios, numAlternativas, nomesAlternativas, vetorMatrizesNormalizadas, vetorVetoresPrioridades, vetorValoresProprios, vetorIC, vetorRC, vetorMatrizesComparacao, mPrioridade, vPrioridadeComposta, vetorIndicesCriteriosCortados);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        LogErros.registarErro(logErros, "O ficheiro não corresponde a um fioheiro de input do método AHP");
                    }
                }
            } else if (metodo.equalsIgnoreCase("m2")) {
                if (args.length > 3) {
                    LogErros.registarErro(logErros, "Existem argumentos em excesso.");
                } else {
                    String tipoFicheiro = tipoFicheiro(args[1]);
                    if (tipoFicheiro.equals("TOPSIS")) {

                        String nomeFicheiro = args[1];
                        String nomeFicheiroOutput = args[2];
                        int[] temporario = TOPSIS.numCriterios_Alternativas(nomeFicheiro);
                        int numCriteriosT = temporario[0];
                        int numAlternativasT = temporario[1];
                        String[] nomesCriteriosT = new String[numCriteriosT];
                        String[] nomesAlternativasT = new String[numAlternativasT];
                        double[][] matrizDecisao = new double[numAlternativasT][numCriteriosT];
                        double[] vPesoCriteriosT = new double[numCriteriosT];
                        String[] nomesCriteriosDesordenadosT = new String[numCriteriosT];
                        char[] custoOuBeneficio = new char[numCriteriosT];

                        double[][] normalizada = new double[numAlternativasT][numCriteriosT];
                        double[][] normalizadaPesada = new double[numAlternativasT][numCriteriosT];
                        double[][] sipSin = new double[2][numCriteriosT];
                        double[] proximidade = new double[numAlternativasT];
                        String[] nomesAlternativasOrdenadas = new String[numCriteriosT];

                        if (!TOPSIS.leituraDados(matrizDecisao, nomesCriteriosT, nomesAlternativasT, vPesoCriteriosT, nomeFicheiro, numCriteriosT, nomesCriteriosDesordenadosT, custoOuBeneficio, logErros, tipoFicheiro)) {
                            LogErros.fecharFileLogErros(logErros);
                        } else {
                            TOPSIS.ordenarCustoBeneficio(nomesCriteriosDesordenadosT, custoOuBeneficio, nomesCriteriosT, numCriteriosT);

                            nomesAlternativasOrdenadas = TOPSIS.metodoTOPSIS(normalizada, normalizadaPesada, sipSin, proximidade, nomesAlternativasOrdenadas, matrizDecisao, numCriteriosT, numAlternativasT, vPesoCriteriosT, custoOuBeneficio, nomesAlternativasT);
                            if (proximidade[0] != proximidade[1]) {
                                escreverDadosTOPSIS(nomeFicheiroOutput, matrizDecisao, vPesoCriteriosT, nomesCriteriosT, normalizada, normalizadaPesada, sipSin, proximidade, nomesAlternativasOrdenadas);
                            } else {
                                LogErros.registarErro(logErros, "O método não é adequado pois existe mais do que uma alternativa ideal");
                            }
                        }
                    } else {
                        LogErros.registarErro(logErros, "O ficheiro não corresponde a um fioheiro de input do método TOPSIS");
                    }
                }
            } else {
                LogErros.registarErro(logErros, "O primeiro argumento deve ser \"m1\"(aproximado) ou \"m2\"(exato)!");
            }
        }
        LogErros.fecharFileLogErros(logErros);
    }

    /**
     * Descobre se o ficheiro recebido corresponde ao método AHP ou TOPSIS
     *
     * @param nomeFicheiro - nome do ficheiro a analisar
     * @return o tipo de ficheiro ou inválido no caso de não conseguir
     * determinar
     * @throws FileNotFoundException
     */
    public static String tipoFicheiro(String nomeFicheiro) throws FileNotFoundException {
        Scanner inputF = new Scanner(new File(nomeFicheiro));
        while (inputF.hasNext()) {

            String linha = inputF.nextLine();

            if (linha.length() > 0) {

                if (linha.substring(0, 3).equalsIgnoreCase("md_")) {
                    return "TOPSIS";
                } else if (linha.substring(0, 3).equalsIgnoreCase("mc_")) {
                    return "AHP";
                }

            }
        }
        return "Invalido";
    }

    /**
     * Escreve todos os dados relativos ao método AHP (matrizes de comparação,
     * as matrizes de comparação normalizadas, o IR, o RC, o maior valor próprio
     * de cada matriz com as respetivas prioridades relativas, o vetor de
     * prioridade composta e a alternativa escolhida)
     *
     * @param indice - posição da alternativa escolhida no vetor que contém o
     * nome das alternativas
     * @param nomeFicheiroOutput - ficheiro (escolhido pelo utilizador) onde são
     * guardados os dados
     * @param numCriterios - número de critérios dos quais as alternativas
     * dependem
     * @param nomesCriterios - vetor que contém o nome de cada critério
     * @param numAlternativas - número de alternativas a comparar
     * @param nomesAlternativas - vetor que contém o nome de cada alternativa
     * @param vetorMatrizesNormalizadas - array tridimensional que contém todas
     * as matrizes de comparação normalizadas
     * @param vetorVetoresPrioridades - array bidimensional que contém o vetor
     * prioridades de cada matriz
     * @param vetorValoresProprios - array que contém o valor próprio de cada
     * matriz
     * @param vetorIC - array que contém o índice de consistência de cada matriz
     * @param vetorRC - array que contém a razão de consistência de cada matriz
     * @param vetorMatrizesComparacao - array tridimensional que contém todas as
     * matrizes de comparação
     * @param mPrioridade - matriz das prioridades relativas
     * @param vPrioridadeComposta - vetor que contém a prioridade de cada
     * alternativa (a sua pontuação final)
     * @param vetorIndicesCriteriosCortados vetor com os indices dos criterios
     * cortados por estarem abaixo do limiar
     * @throws FileNotFoundException
     */
    public static void escreverDadosAHP(int indice, String nomeFicheiroOutput, int numCriterios, String[] nomesCriterios, int numAlternativas, String[] nomesAlternativas, double[][][] vetorMatrizesNormalizadas, double[][] vetorVetoresPrioridades, double[] vetorValoresProprios, double[] vetorIC, double[] vetorRC, double[][][] vetorMatrizesComparacao, double[][] mPrioridade, double[] vPrioridadeComposta, int[] vetorIndicesCriteriosCortados) throws FileNotFoundException {
        boolean flag = true;

        Formatter outFicheiro = new Formatter(new File(nomeFicheiroOutput));
        outFicheiro.format("Atributos cortados:\n");
        out.format("Atributos cortados:\n");

        for (int j = 0; j < numCriterios; j++) {
            if (Utilitarios.valorPresenteEmVetor(j, vetorIndicesCriteriosCortados)) {
                outFicheiro.format(nomesCriterios[j] + "\n");
                out.format(nomesCriterios[j] + "\n");
                flag = false;
            }
        }
        if (flag) {
            outFicheiro.format("\nNenhum\n");
            out.format("\nNenhum\n");

        }

        //////////////////////////////////////////////////////////////////////////////////
        outFicheiro.format("\n\nPara a %dª matriz\n", (1));

        outFicheiro.format("\n\nMatriz de comparação de critérios\n");
        out.format("\n\nMatriz de comparação de critérios\n");

        Utilitarios.escreverMatrizSemZeros(outFicheiro, vetorMatrizesComparacao[0]);
        Utilitarios.escreverMatrizSemZeros(out, vetorMatrizesComparacao[0]);

        outFicheiro.format("\n\nMatriz normalizada\n");
        Utilitarios.escreverMatrizSemZeros(outFicheiro, vetorMatrizesNormalizadas[0]);

        outFicheiro.format("\n\nMaior valor próprio\n%f", vetorValoresProprios[0]);

        outFicheiro.format("\n\nIR\n%f", TABELA_IR[vetorMatrizesComparacao[0].length - 1]);

        outFicheiro.format("\n\nRC\n%f", vetorRC[0]);
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        for (int i = 1; i < numCriterios + 1; i++) {
            if (!Utilitarios.valorPresenteEmVetor(i - 1, vetorIndicesCriteriosCortados)) {

                outFicheiro.format("\n\nPara a %dª matriz\n", (i + 1));

                outFicheiro.format("\n\nMatriz de comparação do critério %s\n", nomesCriterios[i - 1]);
                out.format("\n\nMatriz de comparação do critério %s\n", nomesCriterios[i - 1]);

                escreverMatriz(outFicheiro, vetorMatrizesComparacao[i]);
                escreverMatriz(out, vetorMatrizesComparacao[i]);

                outFicheiro.format("\n\nMatriz normalizada\n");
                escreverMatriz(outFicheiro, vetorMatrizesNormalizadas[i]);

                outFicheiro.format("\n\nMaior valor próprio\n%f", vetorValoresProprios[i]);

                outFicheiro.format("\n\nIR\n%f", TABELA_IR[vetorMatrizesComparacao[i].length - 1]);

                outFicheiro.format("\n\nRC\n%f", vetorRC[i]);
            }
        }

        outFicheiro.format("\n\nMatriz de prioridades relativas\n");
        escreverMatriz(outFicheiro, mPrioridade);

        outFicheiro.format("\n\nVetor de prioridade composta\n");
        out.format("\n\nVetor de prioridade composta\n");
        escreverVetor(outFicheiro, vPrioridadeComposta);
        escreverVetor(out, vPrioridadeComposta);

        outFicheiro.format("\n\nAlternativa escolhida\n");
        out.format("\n\nAlternativa escolhida\n");
        outFicheiro.format(nomesAlternativas[indice]);
        out.format(nomesAlternativas[indice]);

        outFicheiro.close();

    }

    /**
     * Escreve todos os dados relativos ao método TOPSIS (vetor dos pesos de
     * entrada, a matriz de decisão, a matriz normalizada, a matriz normalizada
     * pesada, a solução ideal, a solução ideal negativa, a distancia entre cada
     * alternativa e as soluções ideal e ideal negativa, o vetor com as
     * proximidades relativa à solução ideal e a alternativa escolhida)
     *
     * @param nomeFicheiroOutput - ficheiro (escolhido pelo utilizador) onde são
     * guardados os dados
     * @param matrizDecisao - matriz que contém a importância relativa de cada
     * alternativa relativa a cada um dos critérios
     * @param vPesoCriteriosT - vetor que contém o peso de cada critério(este
     * vetor normalizada os valores dados no ficheiro de input se a soma destes
     * não for um)
     * @param nomesCriteriosT - vetor que contém o nome de cada critério
     * @param normalizada - matriz de decisão normalizada (cada valor passa a
     * ser o quociente desse valor pela raiz da soma dos quadrados dos valores
     * da coluna a que pertence)
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto
     * dos valores da matriz de decisão normalizada pelo peso do critério
     * associado)
     * @param sipSin - matriz cujo primeiro vetor é a solução ideal positiva e
     * cujo segundo vetor é a solução ideal negativa (para cada alternativa)
     * @param proximidade - vetor que contém a proximidade delativa de cada
     * alternativa à solução ideal
     * @param nomesAlternativasOrdenadas - vetor que contém o nome de cada
     * alternativa, ordenadas por ordem decrescente de proximidade relativa
     * @throws FileNotFoundException
     */
    public static void escreverDadosTOPSIS(String nomeFicheiroOutput, double[][] matrizDecisao, double[] vPesoCriteriosT, String[] nomesCriteriosT, double[][] normalizada, double[][] normalizadaPesada, double[][] sipSin, double[] proximidade, String[] nomesAlternativasOrdenadas) throws FileNotFoundException {

        Formatter outFicheiro = new Formatter(new File(nomeFicheiroOutput)); //args[1]

        outFicheiro.format("\n\nVetor dos Pesos\n");
        escreverVetorStrings(outFicheiro, nomesCriteriosT);
        escreverVetor(outFicheiro, vPesoCriteriosT);

        out.format("\n\nVetor dos Pesos\n");
        escreverVetorStrings(out, nomesCriteriosT);
        escreverVetor(out, vPesoCriteriosT);

        outFicheiro.format("\n\nMatriz de decisão\n");
        escreverMatriz(outFicheiro, matrizDecisao);

        out.format("\n\nMatriz de decisão\n");
        escreverMatriz(out, matrizDecisao);

        outFicheiro.format("\n\nMatriz normalizada\n");
        escreverMatriz(outFicheiro, normalizada);

        outFicheiro.format("\n\nMatriz normalizada pesada\n");
        escreverMatriz(outFicheiro, normalizadaPesada);

        out.format("\n\nMatriz normalizada pesada\n");
        escreverMatriz(out, normalizadaPesada);

        outFicheiro.format("\n\nSolução ideal positiva\n");
        escreverVetor(outFicheiro, sipSin[0]);

        outFicheiro.format("\n\nSolução ideal negativa\n");
        escreverVetor(outFicheiro, sipSin[1]);

        outFicheiro.format("\n\nVetor proximidades relativas à solução ideal\n");
        escreverVetorStringsFormat(outFicheiro, nomesAlternativasOrdenadas);
        escreverVetor(outFicheiro, proximidade);

        out.format("\n\nVetor proximidades relativas à solução ideal\n");
        Utilitarios.escreverVetorStringsFormat(out, nomesAlternativasOrdenadas);
        escreverVetor(out, proximidade);

        outFicheiro.format("\n\nAlternativa escolhida\n%s\n", nomesAlternativasOrdenadas[0]);

        out.format("\n\nAlternativa escolhida\n%s\n", nomesAlternativasOrdenadas[0]);

        outFicheiro.close();
    }

}
