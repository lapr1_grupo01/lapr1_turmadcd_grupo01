package projetofinallapr1;

import java.util.Formatter;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;

/**
 * Testa todos os métodos de processamento das outras classes
 */
public class Testes {

    private static final double TOLERANCIA_MILESIMA = 0.001;

    public static void main(String[] args) {

        //
        //AHP DADOS ESPERADOS
        //
        //      [-0.488  0.314  0.599]
        //vd[0] [-0.853  0.633 -0.999]
        //      [-0.186 -0.258 -0.011]
        //
        //      [3.018  0.000  0.000]
        //vd[1] [0.000 -0.009  0.235]
        //      [0.000 -0.235 -0.009]
        String[] nomesCriterios = {"Estilo", "Confiabilidade", "Consumo"};
        int numAlternativas = 4;
        int numCriterios = 3;
        int coluna = 0;
        int colunaEsperada = 0;
        int indice = 0;
        int indiceEsperado = 3;
        int[] vetorIndicesCriteriosCortados = {2};
        double valorProprio = 3.018;
        double icEsperado = 0.009;
        double rcEsperado = 0.016;
        double limiar1 = 0.2;
        double limiar2 = 0.0001;
        double limiarConsistencia = 0.1;
        double[] somaColunaEsperada = {3.3333333, 1.75, 8.0};
        double[] vProprioEsperado = {-0.488, -0.853, -0.186};
        double[] vProprioNormalizadoExatoEsperado = {0.320, 0.559, 0.122};
        double[] vProprioNormalizadoAproximadoEsperado = {0.320, 0.557, 0.123};
        double[] vProprioNormalizadoAproximadoEsperadoLimiar = {0.365, 0.635, 0};
        double[] vPrioridadeComposta = {0.278, 0.230, 0.072, 0.422};
        double[] vPesoCriterios = new double[numCriterios];
        double[][] mComparacao = {{1, 0.5, 3}, {2, 1, 4}, {0.3333333, 0.25, 1}};
        double[][] mComparacaoEstilo = {{1, 0.25, 4, 0.1666666}, {4, 1, 4, 0.25}, {0.25, 0.25, 1, 0.2}, {6, 4, 5, 1}};
        double[][] mComparacaoConfiabilidade = {{1, 2, 5, 1}, {0.5, 1, 4, 0.25}, {0.2, 0.333333, 1, 0.25}, {1, 0.5, 4, 1}};
        double[][] mComparacaoConsumo = {{1, 0.25, 4, 0.1666666}, {4, 1, 4, 0.25}, {0.25, 0.25, 1, 0.2}, {6, 4, 5, 1}};
        double[][] mNormalizadaEsperada = {{0.300, 0.286, 0.375}, {0.600, 0.571, 0.500}, {0.100, 0.143, 0.125}};
        double[][] mPrioridade = {{0.131, 0.412, 0.131}, {0.244, 0.208, 0.244}, {0.066, 0.083, 0.066}, {0.559, 0.297, 0.559}};
        double[][] mPrioridadeEsperada = {{0.131, 0.412, 0.131}, {0.244, 0.208, 0.244}, {0.066, 0.083, 0.066}, {0.559, 0.297, 0.559}};
        double[][] mPrioridadeEsperadaExata = {{0.116, 0.405, 0.116}, {0.247, 0.215, 0.247}, {0.060, 0.081, 0.060}, {0.577, 0.299, 0.577}};
        double[][][] vecPrincipal = {mComparacao, mComparacaoEstilo, mComparacaoConfiabilidade, mComparacaoConsumo};
        Matrix matrizComparacao = Matrix.from2DArray(mComparacao);
        EigenDecompositor decompositor = new EigenDecompositor(matrizComparacao);
        Matrix vd[] = decompositor.decompose();

        //
        //AHP TESTES
        //
        System.out.println("\n---------------------------------------AHP---------------------------------------");

        teste_normalizada(mComparacao, somaColunaEsperada, mNormalizadaEsperada);

        teste_colunaDoValorProprio(vd, colunaEsperada);

        teste_vetorProprio(vd, coluna, vProprioEsperado);

        teste_normalizarVetor(vProprioEsperado, vProprioNormalizadoExatoEsperado);

        teste_calculoIC(valorProprio, numCriterios, icEsperado);

        teste_calculoRC(icEsperado, numCriterios, rcEsperado, limiarConsistencia);

        teste_obterVetorProprioNormalizado(vd, vProprioNormalizadoExatoEsperado);

        teste_vetorPrioridades(mNormalizadaEsperada, indice, limiar2, numCriterios, vProprioNormalizadoAproximadoEsperado, vetorIndicesCriteriosCortados, nomesCriterios);

        teste_valorProprio(numCriterios, numCriterios, mComparacao, vProprioNormalizadoAproximadoEsperado, valorProprio);

        teste_indiceAlternativaEscolhida(vPrioridadeComposta, indiceEsperado);

        teste_vetorPesoCriteriosEMatrizPrioridade(mPrioridadeEsperada, vecPrincipal, numCriterios, numAlternativas, vPesoCriterios, mPrioridade, "a", limiar1, vetorIndicesCriteriosCortados, nomesCriterios);

        teste_vetorPesoCriteriosEMatrizPrioridade(mPrioridadeEsperadaExata, vecPrincipal, numCriterios, numAlternativas, vPesoCriterios, mPrioridade, "e", limiar1, vetorIndicesCriteriosCortados, nomesCriterios);

        teste_aplicaLimiarCriterios(mNormalizadaEsperada, limiar1, numCriterios, vProprioNormalizadoAproximadoEsperado, vetorIndicesCriteriosCortados, nomesCriterios, vProprioNormalizadoAproximadoEsperadoLimiar);

        System.out.println("\n---------------------------------------TOPSIS---------------------------------------");

        //
        //TOPSIS
        //
        int numAlternativasT = 4;
        int numCriteriosT = 4;

        double distanciaEsperada1 = 0.029;
        double distanciaEsperada2 = 0.085;
        double raizSomatorioQuadradosEsperada = 15.166;

        double[] vetorProximidadeRelativaEsperado = {0.743, 0.404, 0.176, 0.441};
        double[] vetorProximidadeOrdenadaEsperado = {0.743, 0.441, 0.404, 0.176};
        double[] vPesoCriteriosT = {0.1, 0.4, 0.3, 0.2};

        char[] custoOuBeneficio = {'b', 'b', 'b', 'c'}; //b = beneficio   c = custo
        String[] vetorNomesAlternativasEsperados = {"Civic", "Mazda", "Saturn", "Ford"};
        String[] nomesAlternativasT = {"Civic", "Saturn", "Ford", "Mazda"};

        double[][] matrizNormalizadaEsperada = {{0.462, 0.614, 0.545, 0.528}, {0.528, 0.477, 0.484, 0.462}, {0.593, 0.409, 0.484, 0.593}, {0.396, 0.477, 0.484, 0.396}};
        double[][] matrizNormalizadaPesadaEsperada = {{0.046, 0.246, 0.163, 0.106}, {0.053, 0.191, 0.145, 0.092}, {0.059, 0.164, 0.145, 0.119}, {0.040, 0.191, 0.145, 0.079}};
        double[][] sipSinEsperado = {{0.059, 0.246, 0.163, 0.079}, {0.040, 0.164, 0.145, 0.119}};
        double[][] matrizDecisao = {{7, 9, 9, 8}, {8, 7, 8, 7}, {9, 6, 8, 9}, {6, 7, 8, 6}};

        double[][] normalizada = new double[numAlternativasT][numCriteriosT];
        double[][] normalizadaPesada = new double[numAlternativasT][numCriteriosT];
        double[][] sipSin = new double[2][numCriteriosT];
        double[] proximidade = new double[numAlternativasT];

        //
        //TOPSIS TESTES
        //
        teste_raizSomatorioQuadrados(matrizDecisao, coluna, numAlternativasT, raizSomatorioQuadradosEsperada);

        teste_criarEPreencherMatrizNormalizada(matrizDecisao, numCriteriosT, numAlternativasT, normalizada, matrizNormalizadaEsperada);

        teste_criarEPreencherMatrizNormalizadaPesada(normalizada, vPesoCriteriosT, numCriteriosT, numAlternativasT, normalizadaPesada, matrizNormalizadaPesadaEsperada);

        teste_sipSin(normalizadaPesada, numCriteriosT, numAlternativasT, custoOuBeneficio, sipSin, sipSinEsperado);

        teste_distancia(0, sipSinEsperado, 0, normalizadaPesada, numCriteriosT, distanciaEsperada1);

        teste_distancia(0, sipSinEsperado, 1, normalizadaPesada, numCriteriosT, distanciaEsperada2);

        teste_proximidadeRelativa(sipSin, normalizadaPesada, numCriteriosT, numAlternativasT, proximidade, vetorProximidadeRelativaEsperado);

        teste_ordenarVetoresAlternativas(proximidade, numAlternativasT, nomesAlternativasT, vetorNomesAlternativasEsperados, vetorProximidadeOrdenadaEsperado);

        System.out.println("\n---------------------------------------UTILITARIOS---------------------------------------");
        //
        //UTILITARIOS
        //

        boolean flagEsperada = true;
        int valorIndice = 2;
        int[] vetorIndicesCortados = {2, 3};
        String digitos = "1111";
        String naoDigitos = "11.11";
        String numS = "0.2";
        String[] tempS = {"0.3", "1/4", "0,7"};

        double numD = 0.2;
        double somaVetorEsperada = -1.527;
        double[] vetorDoublesEsperado = {0.3, 0.25, 0.7};
        double[] somaLinhasEsperada = {0.961, 1.671, 0.368};

        //
        //UTILITARIOS TESTES
        //
        teste_converterStringParaDouble(numS, numD);

        teste_validacaoDigito(digitos, true);

        teste_validacaoDigito(naoDigitos, false);

        teste_somaColuna(mComparacao, coluna, somaColunaEsperada);

        teste_somaLinha(mNormalizadaEsperada, coluna, somaLinhasEsperada);

        teste_somaVetor(vProprioEsperado, somaVetorEsperada);

        teste_multiplicacaoMatrizPorVetor(numAlternativas, numCriterios, mPrioridade, vProprioNormalizadoExatoEsperado, vPrioridadeComposta);

        teste_converterArrayStringParaDouble(tempS, vetorDoublesEsperado);

        teste_valorPresenteEmVetor(valorIndice, vetorIndicesCortados, flagEsperada);
    }

    //
    //MÉTODOS TESTE AHP
    //
    private static void teste_normalizada(double[][] mComparacao, double[] somaColunaEsperada, double[][] mNormalizadaEsperada) {
        System.out.print("normalizada: ");
        double[][] normalizada = AHP.normalizada(mComparacao);
        compararMatrizes(normalizada, mNormalizadaEsperada);
    }

    private static void teste_colunaDoValorProprio(Matrix[] vd, int colunaEsperada) {
        System.out.print("colunaDoValorProprio: ");
        int coluna = AHP.colunaMaiorValorProprio(vd);
        compararValoresInt(coluna, colunaEsperada);
    }

    private static void teste_vetorProprio(Matrix[] vd, int coluna, double[] vProprioEsperado) {
        System.out.print("vetorProprio: ");
        double[] vProprio = AHP.vetorProprio(vd, coluna);
        compararVetores(vProprio, vProprioEsperado);
    }

    private static void teste_normalizarVetor(double[] vProprio, double[] vProprioNormalizadoEsperado) {
        System.out.print("normalizarVetor: ");
        double[] vProprioNormalizado = Utilitarios.normalizarVetor(vProprio);
        compararVetores(vProprioNormalizado, vProprioNormalizadoEsperado);
    }

    private static void teste_calculoIC(double valorProprio, int dimensaoMatriz, double icEsperado) {
        System.out.print("calculoIC: ");
        double ic = AHP.calculoIC(valorProprio, dimensaoMatriz);
        compararValores(ic, icEsperado);
    }

    private static void teste_calculoRC(double ic, int dimensaoMatriz, double rcEsperado, double limiarConsistencia) {
        System.out.print("calculoRC: ");
        double rc = AHP.calculoRC(ic, dimensaoMatriz, limiarConsistencia);
        compararValores(rc, rcEsperado);
    }

    private static void teste_obterVetorProprioNormalizado(Matrix[] vd, double[] vProprioNormalizadoEsperado) {
        System.out.print("obterVetorProprioNormalizado: ");
        double[] vetorNormalizado = AHP.obterVetorProprioNormalizado(vd);
        compararVetores(vetorNormalizado, vProprioNormalizadoEsperado);
    }

    private static void teste_vetorPrioridades(double[][] normalizada, int indice, double limiar, int numCriterios, double[] vetorPrioridadesEsperado, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios) {
        System.out.print("vetorPrioridades: ");
        double[] vetorFinal = AHP.vetorPrioridades(normalizada, indice, limiar, numCriterios, vetorIndicesCriteriosCortados, nomesCriterios);

        compararVetores(vetorFinal, vetorPrioridadesEsperado);
    }

    private static void teste_valorProprio(int numAlternativas, int dimensaoVetor, double[][] matriz, double[] vetor, double valorProprioEsperado) {
        System.out.print("valorProprio: ");
        double valorProprio = AHP.valorProprio(numAlternativas, dimensaoVetor, matriz, vetor);
        compararValores(valorProprio, valorProprioEsperado);
    }

    private static void teste_indiceAlternativaEscolhida(double[] vPrioridadeComposta, int indiceEsperado) {
        System.out.print("indiceAlternativaEscolhida: ");
        int indice = AHP.indiceAlternativaEscolhida(vPrioridadeComposta);
        compararValoresInt(indice, indiceEsperado);
    }

    public static void teste_vetorPesoCriteriosEMatrizPrioridade(double[][] mPrioridadeEsperada, double[][][] vecPrincipal, int numCriterios, int numAlternativas, double[] vPesoCriterios, double[][] mPrioridade, String opcao, double limiar, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios) {

        System.out.print("vetorPesoCriteriosEMatrizPrioridade(aproximado e exato): ");
        mPrioridade = AHP.vetorPesoCriteriosEMatrizPrioridade(vecPrincipal, numCriterios, numAlternativas, vPesoCriterios, mPrioridade, opcao, limiar, vetorIndicesCriteriosCortados, nomesCriterios);
        compararMatrizes(mPrioridade, mPrioridadeEsperada);

    }

    public static void teste_aplicaLimiarCriterios(double[][] normalizada, double limiar, int numCriterios, double[] vetorProprio, int[] vetorIndicesCriteriosCortados, String[] nomesCriterios, double[] vetorProprioEsperado) {
        System.out.print("aplicaLimiarCriterios: ");
        double[] vetorProprioFinal = AHP.aplicaLimiarCriterios(normalizada, limiar, numCriterios, vetorProprio, vetorIndicesCriteriosCortados, nomesCriterios);
        Formatter out = new Formatter(System.out);

    }

    //
    //MÉTODOS TESTES TOPSIS
    //
    public static void teste_raizSomatorioQuadrados(double[][] matrizDecisao, int coluna, int numAlternativasT, double raizSomatorioQuadradosEsperada) {
        System.out.print("raizSomatorioQuadrados: ");
        double raizSomatorioQuadrados = TOPSIS.raizSomatorioQuadrados(matrizDecisao, coluna, numAlternativasT);
        compararValores(raizSomatorioQuadrados, raizSomatorioQuadradosEsperada);

    }

    public static void teste_criarEPreencherMatrizNormalizada(double[][] matrizDecisao, int numCriteriosT, int numAlternativasT, double[][] normalizada, double[][] matrizNormalizadaEsperada) {

        System.out.print("criarEPreencherMatrizNormalizada: ");
        TOPSIS.preencherMatrizNormalizada(matrizDecisao, numCriteriosT, numAlternativasT, normalizada);

        compararMatrizes(normalizada, matrizNormalizadaEsperada);

    }

    public static void teste_criarEPreencherMatrizNormalizadaPesada(double[][] normalizada, double[] vPesoCriteriosT, int numCriteriosT, int numAlternativasT, double[][] normalizadaPesada, double[][] matrizNormalizadaPesadaEsperada) {
        System.out.print("criarEPreencherMatrizNormalizadaPesada: ");
        TOPSIS.preencherMatrizNormalizadaPesada(normalizada, vPesoCriteriosT, numCriteriosT, numAlternativasT, normalizadaPesada);

        compararMatrizes(normalizadaPesada, matrizNormalizadaPesadaEsperada);
    }

    public static void teste_sipSin(double[][] normalizadaPesada, int numCriteriosT, int numAlternativasT, char[] custoOuBeneficio, double[][] sipSin, double[][] sipSinEsperado) {
        System.out.print("sipSin: ");
        TOPSIS.sipSin(normalizadaPesada, numCriteriosT, numAlternativasT, custoOuBeneficio, sipSin);

        compararMatrizes(sipSin, sipSinEsperado);
    }

    public static void teste_distancia(int linha, double[][] sipSin, int sipOuSin, double[][] normalizadaPesada, int numCriteriosT, double distanciaEsperada) { //sipOuSin -> sip = 0  sin = 1
        System.out.print("distancia: ");
        double distancia = TOPSIS.distancia(linha, sipSin, sipOuSin, normalizadaPesada, numCriteriosT);
        compararValores(distancia, distanciaEsperada);

    }

    public static void teste_proximidadeRelativa(double[][] sipSin, double[][] normalizadaPesada, int numCriteriosT, int numAlternativasT, double[] proximidade, double[] vetorProximidadeRelativaEsperado) {
        System.out.print("proximidadeRelativa: ");
        TOPSIS.proximidadeRelativa(sipSin, normalizadaPesada, numCriteriosT, numAlternativasT, proximidade);
        compararVetores(proximidade, vetorProximidadeRelativaEsperado);

    }

    public static void teste_ordenarVetoresAlternativas(double[] vetor, int numAlternativasT, String[] nomesAlternativasT, String[] vetorNomesAlternativasEsperados, double[] vetorProximidadeOrdenadaEsperado) {
        System.out.print("ordenarVetoresAlternativas(vetor nomes ordenados): ");
        String[] vetorNomesAlternativas = TOPSIS.ordenarVetoresAlternativas(vetor, numAlternativasT, nomesAlternativasT);
        compararVetoresStrings(vetorNomesAlternativas, vetorNomesAlternativasEsperados);

        System.out.print("ordenarVetoresAlternativas(vetor proximidade ordenado): ");
        compararVetores(vetor, vetorProximidadeOrdenadaEsperado);
    }

    //
    //MÉTODOS DE TESTE UTILITÁRIOS
    //
    public static void teste_converterStringParaDouble(String str, double valorEsperado) {
        System.out.print("converterStringParaDouble: ");
        double valor = Utilitarios.converterStringParaDouble(str);
        compararValores(valor, valorEsperado);
    }

    public static void teste_validacaoDigito(String str, boolean respostaEsperada) {
        System.out.print("validacaoDigito: ");
        boolean resposta = Utilitarios.validacaoDigito(str);
        compararBooleans(resposta, respostaEsperada);
    }

    private static void teste_somaColuna(double[][] mComparacao, int j, double[] somaColunaEsperada) {
        System.out.print("somaColuna: ");
        double soma = Utilitarios.somaColuna(mComparacao, j);
        compararValores(soma, somaColunaEsperada[j]);
    }

    private static void teste_somaLinha(double[][] normalizada, int i, double[] somaLinhaEsperada) {
        System.out.print("somaLinha: ");
        double soma = Utilitarios.somaLinha(normalizada, i);
        compararValores(soma, somaLinhaEsperada[i]);
    }

    private static void teste_somaVetor(double[] vProprio, double somaVetorEsperada) {
        System.out.print("somaVetor: ");
        double soma = Utilitarios.somaVetor(vProprio);
        compararValores(soma, somaVetorEsperada);
    }

    public static void teste_multiplicacaoMatrizPorVetor(int numAlternativas, int numCriterios, double[][] matriz, double[] vetor, double[] vetorEsperado) {
        System.out.print("multiplicacaoMatrizPorVetor: ");
        double[] vetorFinal = Utilitarios.multiplicacaoMatrizPorVetor(matriz, vetor);
        compararVetores(vetorFinal, vetorEsperado);
    }

    public static void teste_converterArrayStringParaDouble(String[] tempS, double[] vetorDoublesEsperado) {
        System.out.print("converterArrayStringParaDouble: ");
        double[] vetorDoubles = Utilitarios.converterArrayStringParaDouble(tempS);
        compararVetores(vetorDoubles, vetorDoublesEsperado);
    }

    public static void teste_valorPresenteEmVetor(int val, int[] vec, boolean flagEsperada) {
        System.out.print("valorPresenteEmVetor: ");
        boolean flag = Utilitarios.valorPresenteEmVetor(val, vec);
        compararBooleans(flag, flagEsperada);
    }

    //
    //MÉTODOS DE COMPARAÇÃO
    //
    public static void compararValores(double val1, double val2) {
        if (val1 < (val2 + TOLERANCIA_MILESIMA) && val1 > (val2 - TOLERANCIA_MILESIMA)) {
            System.out.println("Passou");
        } else {
            System.out.println("Falhou");
        }
    }

    public static void compararBooleans(boolean bool1, boolean bool2) {
        if (bool1 == bool2) {
            System.out.println("Passou");
        } else {
            System.out.println("Falhou");
        }
    }

    public static void compararValoresInt(int val1, int val2) {
        if (val1 == val2) {
            System.out.println("Passou");
        } else {
            System.out.println("Falhou");
        }
    }

    public static void compararVetores(double[] vet1, double[] vet2) {
        int i;

        for (i = 0; i < vet1.length; i++) {
            if (vet1[i] > (vet2[i] + TOLERANCIA_MILESIMA) || vet1[i] < (vet2[i] - TOLERANCIA_MILESIMA)) {
                System.out.println("Falhou");
                i = vet1.length;
            }

        }
        if (i != vet1.length + 1) {
            System.out.println("Passou");
        }
    }

    public static void compararVetoresStrings(String[] vet1, String[] vet2) {
        int i;

        for (i = 0; i < vet1.length; i++) {
            if (!vet1[i].equals(vet2[i])) {
                System.out.println("Falhou");
                i = vet1.length;
            }

        }
        if (i != vet1.length + 1) {
            System.out.println("Passou");
        }
    }

    public static void compararMatrizes(double[][] mat1, double[][] mat2) {
        int j = 0;
        for (int i = 0; i < mat1.length; i++) {
            for (j = 0; j < mat1[0].length; j++) {
                if (mat1[i][j] > (mat2[i][j] + TOLERANCIA_MILESIMA) || mat1[i][j] < (mat2[i][j] - TOLERANCIA_MILESIMA)) {
                    System.out.println("Falhou");
                    j = mat1[0].length;
                    i = mat1.length;
                }
            }
        }
        if (j != mat1[0].length + 1) {
            System.out.println("Passou");
        }
    }

}
