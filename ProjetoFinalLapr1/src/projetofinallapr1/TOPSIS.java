package projetofinallapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Leitura de dados e processamento relativo ao método
 * matemático TOPSIS
 */
public class TOPSIS {

    private static final int MENOR_NORMALIZADA_PESADA = -1;
    private static final int MAIOR_NORMALIZADA_PESADA = 2;
    private final static String FILE_LOG_ERROS = "Erros.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        
        
        //String[] argumentos = {"M2", "inputTOPSIS.txt", "output.txt"};

        //TESTES LEITURA
//        int numAlternativasT = 4;
//        int numCriteriosT = 4;
//
//        String[] nomesCriteriosT = {"Estilo", "Confiabilidade", "Consumo", "Custo"};
//        String[] nomesAlternativasT = {"Civic", "Saturn", "Ford", "Mazda"};
//
//        double[][] matrizDecisao = {{7, 9, 9, 8}, {8, 7, 8, 7}, {9, 6, 8, 9}, {6, 7, 8, 6}};
//
        //double[] vPesoCriteriosT = {0.1, 0.4, 0.3, 0.2};
//
//        char[] custoOuBeneficio = {'b', 'b', 'b', 'c'}; //b = beneficio   c = custo
        
        
//        //LEITURA
//        String nomeFicheiro = args[1];
//        int[] temporario = numCriterios_Alternativas(nomeFicheiro);
//        int numCriteriosT = temporario[0];
//        int numAlternativasT = temporario[1];
//        String[] nomesCriteriosT = new String[numCriteriosT];
//        String[] nomesAlternativasT = new String[numAlternativasT];
//        double[][] matrizDecisao = new double[numAlternativasT][numCriteriosT];
//        double[] vPesoCriteriosT = new double[numCriteriosT];
//        String[] nomesCriteriosDesordenadosT = new String[numAlternativasT];
//        char[] custoOuBeneficio = new char[numAlternativasT];
//
//        if(!leituraDados(matrizDecisao, nomesCriteriosT, nomesAlternativasT, vPesoCriteriosT, nomeFicheiro, numCriteriosT, nomesCriteriosDesordenadosT, custoOuBeneficio , logErros)){
//            LogErros.fecharFileLogErros(logErros);
//        } else {
//            ordenarCustoBeneficio(nomesCriteriosDesordenadosT, custoOuBeneficio, nomesCriteriosT, numCriteriosT);

            //TESTES ESCRITA LEITURA
//            ProjetoFinalLapr1.escreverMatriz(out, matrizDecisao);
//            ProjetoFinalLapr1.escreverVetorStrings(out, nomesCriteriosT);
//            ProjetoFinalLapr1.escreverVetorStrings(out, nomesAlternativasT);
//            ProjetoFinalLapr1.escreverVetor(out, vPesoCriteriosT);
//            ProjetoFinalLapr1.escreverVetorChars(out, custoOuBeneficio);


            //TESTES ESCRITA CÁLCULOS
//            System.out.println("--------------DECISAO--------------");
//            Utilitarios.escreverMatriz(out, matrizDecisao);
//            System.out.println("----------------PESADA---------------");
//            Utilitarios.escreverMatriz(out, normalizadaPesada);
//            System.out.println("---------------SipSin-----------------");
//            Utilitarios.escreverMatriz(out, sipSin);
//            System.out.println("---------------Proximidade-----------------");
//            Utilitarios.escreverVetor(out, proximidade);
//            System.out.println("---------------Alternativa-----------------");
//            System.out.println(nomesAlternativasOrdenadas[0]);
    }

    /**
     * Analisa o ficheiro de input para descobrir o número de critérios e o número de alternativas
     * @param nomeFicheiro - ficheiro (escolhido pelo utilizador) de onde são recebidos os dados
     * @return um vetor com o número de critérios e o número de alternativas
     * @throws FileNotFoundException 
     */
    public static int[] numCriterios_Alternativas(String nomeFicheiro) throws FileNotFoundException {
        Scanner inputF = new Scanner(new File(nomeFicheiro));
        int[] vecNumCriAlt = new int[2];
        int varAlt = 0, varCri = 0, numAlt = 0, numCri = 0;
        while (inputF.hasNext()) {
            String linha = inputF.nextLine();
            if (linha.length() > 0) {
                if (linha.trim().substring(0, 4).equals("alt ") && varAlt == 0) {
                    numAlt = linha.trim().split("  ").length - 1;
                    varAlt = 1;
                } else if (linha.trim().substring(0, 4).equals("crt ") && varCri == 0) {
                    numCri = linha.trim().split("  ").length - 1;
                    varCri = 1;
                }

            }
        }
        vecNumCriAlt[0] = numCri;
        vecNumCriAlt[1] = numAlt;

        return vecNumCriAlt;
    }

    /**
     * Analisa o ficheiro de input para descobrir a matriz de decisão, os nomes dos critérios e das alternativas, o peso de cada critério e o valor de custo ou benefício de cada um desses critérios
     * @param matrizDecisao - matriz que contém a importância relativa de cada alternativa relativa a cada um dos critérios
     * @param nomesCriteriosT - vetor que contém o nome de cada critério
     * @param nomesAlternativasT - vetor que contém o nome de cada alternativa
     * @param vPesoCriteriosT - vetor que contém o peso de cada critério(este vetor normalizada os valores dados no ficheiro de input se a soma destes não for um)
     * @param nomeFicheiro - ficheiro (escolhido pelo utilizador) de onde são recebidos os dados
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param nomesCriteriosDesordenadosT - vetor que contém o nome de cada alternativa, ordenados por ordem de referência nos cabeçalhos de custo ou benefício
     * @param custoOuBeneficio - o valor de custo ou benefício de cada um dos critérios
     * @param logErros - Formatter que regista os erros num ficheiro à parte
     * @return a validade do ficheiro (se contém todos os dados necessários ou não e se algum desses dados é inválido)
     * @throws FileNotFoundException 
     */
    public static boolean leituraDados(double[][] matrizDecisao, String[] nomesCriteriosT, String[] nomesAlternativasT, double[] vPesoCriteriosT, String nomeFicheiro, int numCriteriosT, String[] nomesCriteriosDesordenadosT, char[] custoOuBeneficio, Formatter logErros, String tipoFicheiro) throws FileNotFoundException {

        int cont = 0;
        boolean pesos = false, beneficioECusto = false;
       
        Scanner inputF = new Scanner(new File(nomeFicheiro));

        while (inputF.hasNext()) {
            String linha = inputF.nextLine();

            if (linha.length() > 0) {

                if (linha.trim().substring(0, 3).equals("md_")) {
                    
                    linha = inputF.nextLine();

                    for (int i = 0; i < nomesCriteriosT.length; i++) {
                        String[] elementosLinha = linha.trim().split("  ");
                        nomesCriteriosT[i] = elementosLinha[i + 1];
                    }
                    
                    linha = inputF.nextLine();
                    
                    for (int i = 0; i < nomesAlternativasT.length; i++) {
                        String[] elementosLinha = linha.trim().split("  ");
                        nomesAlternativasT[i] = elementosLinha[i + 1];
                    }
                    
                    for (int i = 0; i < nomesAlternativasT.length; i++) {
                        linha = inputF.nextLine();
                        if (Utilitarios.guardarEmMemoria(logErros, linha, numCriteriosT, i, matrizDecisao, tipoFicheiro) == -1) {
                        return false;
                        }
                        
                    }
                    

                    
                }else if(linha.trim().substring(0, 4).equals("vec_")){
                        pesos = true;
                        linha = inputF.nextLine();
                        String[] elementosLinha = linha.trim().split(" +");
                        for (int i = 0; i < elementosLinha.length; i++) {
                            vPesoCriteriosT[i] = Utilitarios.converterStringParaDouble(elementosLinha[i]);
                        }
                        if(Utilitarios.somaVetor(vPesoCriteriosT)!=1){
                            double[]vPesoCriteriosTNormalizado=Utilitarios.normalizarVetor(vPesoCriteriosT);
                                for (int i = 0; i < vPesoCriteriosT.length; i++) {
                                    vPesoCriteriosT[i]=vPesoCriteriosTNormalizado[i];
                                }
                            }

                } else if (linha.trim().substring(0, 4).equals("crt_")) {
                    beneficioECusto = true;
                    if (linha.trim().charAt(4) == ('b')) {
                        for (int k = 1; k < linha.trim().split("  ").length; k++) {
                            if (cont >= numCriteriosT) {
                                LogErros.registarErro(logErros, "Os critérios listados em benefícios e custos não correspondem aos listados na matriz de decisão");
                                return false;
                            } else {
                                nomesCriteriosDesordenadosT[cont] = linha.trim().split("  ")[k];
                                custoOuBeneficio[cont] = 'b';
                                cont++;
                            }

                        }
                    } else {
                        for (int i = 1; i < linha.trim().split("  ").length; i++) {
                            if (cont >= numCriteriosT) {
                                LogErros.registarErro(logErros, "Os critérios listados em benefícios e custos não correspondem aos listados na matriz de decisão");
                                return false;
                            } else {
                                nomesCriteriosDesordenadosT[cont] = linha.trim().split("  ")[i];
                                custoOuBeneficio[cont] = 'c';
                                cont++;
                            }

                        }
                        

                    }

                }
            }

        }
        
        if (!beneficioECusto){
            for (int i = 0; i < numCriteriosT; i++) {
                custoOuBeneficio[i] = 'b';
            }
        }
        
        if (!pesos){
            for (int i = 0; i < numCriteriosT; i++) {
                vPesoCriteriosT[i] = (double)1/numCriteriosT;
            }
        }
        return true;

    }

    /**
     * Ordena o vetor custoOuBeneficio conforme a ordem dos critério referidos na matriz de decisão
     * @param nomesCriteriosDesordenadosT - vetor que contém o nome de cada alternativa, ordenados por ordem de referência nos cabeçalhos de custo ou benefício
     * @param custoOuBeneficio - o valor de custo ou benefício de cada um dos critérios
     * @param nomesCriteriosT - vetor que contém o nome de cada critério
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @throws FileNotFoundException 
     */
    public static void ordenarCustoBeneficio(String[] nomesCriteriosDesordenadosT, char[] custoOuBeneficio, String[] nomesCriteriosT, int numCriteriosT) throws FileNotFoundException {

        Formatter logErros = LogErros.criarEAbrirFileLogErros(FILE_LOG_ERROS);

        String tmp;
        char charTmp;

        for (int i = 0; i < numCriteriosT; i++) {

            if (!nomesCriteriosT[i].equals(nomesCriteriosDesordenadosT[i])) {

                int indice = procurarIndiceIgual(nomesCriteriosDesordenadosT, nomesCriteriosT[i], numCriteriosT);
                if (indice == -1) {
                    LogErros.registarErro(logErros, "Os critérios listados em benefícios e custos não correspondem aos listados na matriz de decisão");
                } else {
                    tmp = nomesCriteriosDesordenadosT[indice];
                    nomesCriteriosDesordenadosT[indice] = nomesCriteriosDesordenadosT[i];
                    nomesCriteriosDesordenadosT[i] = tmp;

                    charTmp =custoOuBeneficio[indice];
                    custoOuBeneficio[indice] = custoOuBeneficio[i];
                    custoOuBeneficio[i] = charTmp;
                }
            }
        }
        LogErros.fecharFileLogErros(logErros);
    }

    /**
     * Verifica que os critérios listados em benefícios e custos correspondem aos listados na matriz de decisão
     * @param nomesCriteriosDesordenadosT - vetor que contém o nome de cada alternativa, ordenados por ordem de referência nos cabeçalhos de custo ou benefício
     * @param nomeCriterio - critério a procurar
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @return a posição do critério no vetor do nome dos critérios e este existir; -1 se não existir
     */
    public static int procurarIndiceIgual(String[] nomesCriteriosDesordenadosT, String nomeCriterio, int numCriteriosT) {

        for (int i = 0; i < numCriteriosT; i++) {
            if (nomeCriterio.equals(nomesCriteriosDesordenadosT[i])) {
                return i;
            }
        }
        return -1;
    }


    /**
     * 
     * @param matrizDecisao - matriz que contém a importância relativa de cada alternativa relativa a cada um dos critérios
     * @param coluna - coluna da matriz de decisão a partir da qual se fazem os cálculos(corresponde a um dos critérios)
     * @param numAlternativasT - número de alternativas a comparar
     * @return a raiz da soma dos quadrados de cada elemento da coluna da matriz de decisão
     */
    public static double raizSomatorioQuadrados(double[][] matrizDecisao, int coluna, int numAlternativasT) {

        double soma = 0;
        for (int i = 0; i < numAlternativasT; i++) {
            soma += Math.pow(matrizDecisao[i][coluna], 2);
        }

        double raiz = Math.sqrt(soma);
        return raiz;
    }
    
    /**
     * Preenche a matriz normalizada(previamente criada) com o quociente do valor da matriz de decisão pela raiz da soma dos quadrados dos valores da coluna a que pertence
     * @param matrizDecisao - matriz que contém a importância relativa de cada alternativa relativa a cada um dos critérios
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param numAlternativasT - número de alternativas a comparar
     * @param normalizada - matriz de decisão normalizada (cada valor passa a ser o quociente desse valor pela raiz da soma dos quadrados dos valores da coluna a que pertence)
     */
    public static void preencherMatrizNormalizada(double[][] matrizDecisao, int numCriteriosT, int numAlternativasT, double[][]normalizada) {


        for (int i = 0; i < numAlternativasT; i++) {
            for (int j = 0; j < numCriteriosT; j++) {
                normalizada[i][j] = matrizDecisao[i][j] / raizSomatorioQuadrados(matrizDecisao, j, numAlternativasT);
            }
        }
    }

    /**
     * Preenche a matriz normalizada pesada(previamente criada) com o produto dos valores da matriz de decisão normalizada pelo peso do critério associado
     * @param normalizada - matriz de decisão normalizada (cada valor passa a ser o quociente desse valor pela raiz da soma dos quadrados dos valores da coluna a que pertence)
     * @param vPesoCriteriosT - vetor que contém o peso de cada critério(este vetor normalizada os valores dados no ficheiro de input se a soma destes não for um)
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param numAlternativasT - número de alternativas a comparar
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto dos valores da matriz de decisão normalizada pelo peso do critério associado)
     */
    public static void preencherMatrizNormalizadaPesada(double[][] normalizada, double[] vPesoCriteriosT, int numCriteriosT, int numAlternativasT, double[][]normalizadaPesada) {


        for (int i = 0; i < numAlternativasT; i++) {
            for (int j = 0; j < numCriteriosT; j++) {
                normalizadaPesada[i][j] = normalizada[i][j] * vPesoCriteriosT[j];
            }
        }
    }

    /**
     * Preenche a matriz sipSin (previamente criada) com solução ideal positiva e a solução ideal negativa de cada alternativa
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto dos valores da matriz de decisão normalizada pelo peso do critério associado)
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param numAlternativasT - número de alternativas a comparar
     * @param custoOuBeneficio - o valor de custo ou benefício de cada um dos critérios
     * @param sipSin - matriz cujo primeiro vetor é a solução ideal positiva e cujo segundo vetor é a solução ideal negativa (para cada alternativa)
     */
    public static void sipSin(double[][] normalizadaPesada, int numCriteriosT, int numAlternativasT, char[] custoOuBeneficio,  double[][] sipSin) {

        double maiorValor = MENOR_NORMALIZADA_PESADA;
        double menorValor = MAIOR_NORMALIZADA_PESADA;

        for (int i = 0; i < numCriteriosT; i++) {
            for (int j = 0; j < numAlternativasT; j++) {

                if (normalizadaPesada[j][i] > maiorValor) {
                    if (custoOuBeneficio[i]==('b')) {
                        sipSin[0][i] = normalizadaPesada[j][i];
                        maiorValor = normalizadaPesada[j][i];
                    } else {
                        sipSin[1][i] = normalizadaPesada[j][i];
                        maiorValor = normalizadaPesada[j][i];
                    }

                } else if (normalizadaPesada[j][i] < menorValor) {
                    if (custoOuBeneficio[i]==('c')) {
                        sipSin[0][i] = normalizadaPesada[j][i];
                        menorValor = normalizadaPesada[j][i];
                    } else {
                        sipSin[1][i] = normalizadaPesada[j][i];
                        menorValor = normalizadaPesada[j][i];
                    }
                }
            }
            maiorValor = MENOR_NORMALIZADA_PESADA;
            menorValor = MAIOR_NORMALIZADA_PESADA;
        }
    }

    /**
     * Calcula a distância entre a solução alternativa e a solução ideal (positiva ou negativa) 
     * @param linha - linha da matriz normalizada pesada a partir da qual se fazem os cálculos(corresponde a uma das alternativas)
     * @param sipSin - matriz cujo primeiro vetor é a solução ideal positiva e cujo segundo vetor é a solução ideal negativa (para cada alternativa)
     * @param sipOuSin - vetor da matriz sipSin a usar (0 se se pretender a distância à solução ideal positiva ou 1 se se pretender a distância à solução ideal negativa)
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto dos valores da matriz de decisão normalizada pelo peso do critério associado)
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @return a distância entre a solução alternativa e a solução ideal (positiva ou negativa) 
     */
    public static double distancia(int linha, double[][] sipSin, int sipOuSin, double[][] normalizadaPesada, int numCriteriosT) { //sipOuSin -> sip = 0  sin = 1

        double soma = 0;

        for (int i = 0; i < numCriteriosT; i++) {
            soma += Math.pow((sipSin[sipOuSin][i] - normalizadaPesada[linha][i]), 2);
        }

        double distancia = Math.sqrt(soma);
        return distancia;
    }

    /**
     * Preenche o vetor proximidade (previamente criado) com a proximidade delativa de cada alternativa à solução ideal
     * @param sipSin - matriz cujo primeiro vetor é a solução ideal positiva e cujo segundo vetor é a solução ideal negativa (para cada alternativa)
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto dos valores da matriz de decisão normalizada pelo peso do critério associado)
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param numAlternativasT - número de alternativas a comparar
     * @param proximidade - vetor que contém a proximidade relativa de cada alternativa à solução ideal
     */
    public static void proximidadeRelativa(double[][] sipSin, double[][] normalizadaPesada, int numCriteriosT, int numAlternativasT, double[]proximidade) {

        for (int i = 0; i < numAlternativasT; i++) {
            for (int j = 0; j < numCriteriosT; j++) {
                double distanciaPositiva = distancia(i, sipSin, 0, normalizadaPesada, numCriteriosT);
                double distanciaNegativa = distancia(i, sipSin, 1, normalizadaPesada, numCriteriosT);
                proximidade[i] = distanciaNegativa / (distanciaNegativa + distanciaPositiva);
            }
        }
    }

    /**
     * Ordena os vetores proximidade por ordem decrescente de proximidade relativa à solução ideal e ordena o vetor que contém os nomes das alternativas correspondentes ao valores ordenados, guardando os num novo vetor
     * @param proximidade - vetor que contém a proximidade relativa de cada alternativa à solução ideal
     * @param numAlternativasT - número de alternativas a comparar
     * @param nomesAlternativasT - vetor que contém o nome de cada alternativa
     * @return o vetor que contém o nome de cada alternativa, ordenadas por ordem decrescente de proximidade relativa
     */
    public static String[] ordenarVetoresAlternativas(double[] proximidade, int numAlternativasT, String[] nomesAlternativasT) {

        double tmp;
        String StrTmp;
        
        String[] nomesAlternativasOrdenadas = nomesAlternativasT;
        for (int numerosCounter = 0; numerosCounter < numAlternativasT; numerosCounter++) {

            for (int counterSeguinte = numerosCounter + 1; counterSeguinte < numAlternativasT; counterSeguinte++) {

                if (proximidade[numerosCounter] < proximidade[counterSeguinte]) {

                    tmp = proximidade[numerosCounter];
                    proximidade[numerosCounter] = proximidade[counterSeguinte];
                    proximidade[counterSeguinte] = tmp;

                    StrTmp = nomesAlternativasOrdenadas[numerosCounter];
                    nomesAlternativasOrdenadas[numerosCounter] = nomesAlternativasOrdenadas[counterSeguinte];
                    nomesAlternativasOrdenadas[counterSeguinte] = StrTmp;

                }
            }
        }
        return nomesAlternativasOrdenadas;
    }
    
    /**
     * Realiza todos os cálculos necessário à utilização do método TOPSIS
     * @param normalizada - matriz de decisão normalizada (cada valor passa a ser o quociente desse valor pela raiz da soma dos quadrados dos valores da coluna a que pertence)
     * @param normalizadaPesada - matriz de decisão normalizada pesada(produto dos valores da matriz de decisão normalizada pelo peso do critério associado)
     * @param sipSin - matriz cujo primeiro vetor é a solução ideal positiva e cujo segundo vetor é a solução alternta negativa (para cada alternativa)
     * @param proximidade - vetor que contém a proximidade relativa de cada alternativa à solução ideal
     * @param nomesAlternativasOrdenadas - vetor que contém o nome de cada alternativa, ordenadas por ordem decrescente de proximidade relativa
     * @param matrizDecisao - matriz que contém a importância relativa de cada alternativa relativa a cada um dos critérios
     * @param numCriteriosT - número de critérios dos quais a as alternativas dependem
     * @param numAlternativasT - número de alternativas a comparar
     * @param vPesoCriteriosT - vetor que contém o peso de cada critério(este vetor normalizada os valores dados no ficheiro de input se a soma destes não for um)
     * @param custoOuBeneficio - o valor de custo ou benefício de cada um dos critérios
     * @param nomesAlternativasT - vetor que contém o nome de cada alternativa
     * @return o vetor que contém o nome de cada alternativa, ordenadas por ordem decrescente de proximidade relativa (cujo primeiro valor é a alternativa escolhida)
     */
    public static String[] metodoTOPSIS(double[][] normalizada,double[][] normalizadaPesada,double[][] sipSin, double[]proximidade, String[]nomesAlternativasOrdenadas, double[][] matrizDecisao, int numCriteriosT, int numAlternativasT, double[] vPesoCriteriosT, char[] custoOuBeneficio, String[] nomesAlternativasT){
        
        preencherMatrizNormalizada(matrizDecisao, numCriteriosT, numAlternativasT, normalizada);
        preencherMatrizNormalizadaPesada(normalizada, vPesoCriteriosT, numCriteriosT, numAlternativasT, normalizadaPesada);
        sipSin(normalizadaPesada, numCriteriosT, numAlternativasT, custoOuBeneficio, sipSin);
        proximidadeRelativa(sipSin, normalizadaPesada, numCriteriosT, numAlternativasT, proximidade);
        nomesAlternativasOrdenadas = ordenarVetoresAlternativas(proximidade, numAlternativasT, nomesAlternativasT);
        
        return nomesAlternativasOrdenadas;
    }
    
}